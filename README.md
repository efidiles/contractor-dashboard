# Contractor dashboard
- TODO

# Installation
- Clone the repo
- Run `npm install` in the project's directory
- Use `npm start` to run the production build

# Main technologies used
- React
- Redux
- Webpack
- Electron
- Material UI
- Karma / Enzyme / Sinon

# TODO
- Release tooling
- Add reselect
- Remove .bind and arrow functions
- Take React outside the packaging