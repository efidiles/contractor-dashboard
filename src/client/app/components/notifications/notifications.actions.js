import loggerFactory from '../../utils/logger';

const logger = loggerFactory('From notifications.actions');

export const NOTIFICATIONS_ADD = 'NOTIFICATIONS_ADD';
export const NOTIFICATIONS_REMOVE = 'NOTIFICATIONS_REMOVE';
export const NOTIFICATIONS_CLEAR = 'NOTIFICATIONS_CLEAR';

export const add = (notificationType, description) => {
  logger.debug('Inside add');

  return {
    type: NOTIFICATIONS_ADD,
    notificationType,
    description
  };
};

export const remove = id => {
  logger.debug('Inside remove');

  return {
    type: NOTIFICATIONS_REMOVE,
    id
  };
};

export const clear = notificationType => {
  logger.debug('Inside clear');

  return {
    type: NOTIFICATIONS_CLEAR,
    notificationType
  };
};
